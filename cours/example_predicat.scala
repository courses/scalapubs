
case class Customer( /*...*/
  registrationDate: LocalDateTime
  orders: List[Order]
)

def oldCustomer( c: Customer ): Boolean =
  (LocalDateTime.now - c.registrationDate) >= Year(1)

def boughtALot( c: Customer ): Boolean =
  c.orders.filter( o => (LocalDateTime.now - o.date) <= Months(6) )
    .sum >= 1000

/*def goodCustomer( c: Customer ): Boolean =
  oldCustomer(c) && boughtALot(c)
 */

//def didntBuyALot( c: Customer ): Boolean = ! boughtALot(c)

val didntBuyALot = not( boughtALot )

val goodCustomer = and( oldCustomer, boughtALot )

val nextMarketingOffer = and( oldCustomer, not( boughtALot ) )

