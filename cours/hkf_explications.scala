val f = (i:Int) => (j:Int) => i*j

val g = f(3) : (Int)=>Int
val g = (  (i:Int) => (j:Int) => i*j )(3)
val g =  (j:Int) => 3*j

val x = g(10)
val x = ((j:Int) => 3*j)(10)
val x = (3*10)
val x = 30

val y = ( f(2) )(5)
val y = (  ((i:Int) => (j:Int) => i*j)(2) )(5)
val y = (  (j:Int) => 2*j )(5)
val y = ( 2*5 )
val y = 10


val log = (f:Int=>Int) => { (i:Int) => 
  val j = f(i)
  println( s"INPUT: $i OUTPUT: $j" )
  j
}

val logG = log(g)
val logG = {
  (f:Int=>Int) => { i:Int =>
    val j = f(i)
    println( s"INPUT: $i OUTPUT: $j" )
    j 
  }
}(g)
val logG = {
  (f:Int=>Int) => { i:Int =>
    val j = f(i)
    println( s"INPUT: $i OUTPUT: $j" )
    j 
  }
}( (j:Int) => 3*j )
val logG =  (i:Int) => {
    val j = ( (j:Int) => 3*j )(i)
    println( s"INPUT: $i OUTPUT: $j" )
    j 
  }
val logG =  (i:Int) => {
    val j = ( (k:Int) => 3*k )(i)
    println( s"INPUT: $i OUTPUT: $j" )
    j 
}
val logG =  (i:Int) => {
    val j = 3*i
    println( s"INPUT: $i OUTPUT: $j" )
    j 
}
val logG =  (i:Int) => {
    println( s"INPUT: $i OUTPUT: ${3*i}" )
    3*i
}
val g =  (i:Int) => 3*i
logG(10) == g(10)  //Mais affiche 'INPUT: 10 OUTPUT: 30'



