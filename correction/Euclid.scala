package exo

object Euclid extends App {

  /* Equivalent en Java
   * int gcd( final int i, final int j ) 
   */
  def gcd( i: Int, j: Int ): Int = {
    var a = i
    var b = j
    while( b != 0 ) {
      val t = b
      b = a % t
      a = t
    }
    a
  }

  def gcdRec( i: Int, j: Int ): Int =
    if( j == 0 )
      i
    else
      gcdRec( j, i % j )


  println( gcd( 100, 30 ) )
  println( gcd( 30, 100 ) )
  println( gcd( 12, 3732 ) )
  println( gcd( 1, 3732 ) )
  println( gcd( 25, 3732 ) )

}
