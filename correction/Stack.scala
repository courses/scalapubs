package ch.unige.hepia.tp

import java.util.ArrayList

class Stack[A] {

  private val stack = new ArrayList[A]

  //def this( a: A ) = stack.add(a)

  //Retourne vrai si la pile est vide
  def isEmpty: Boolean = stack.isEmpty

  // Retourne le nombre d'éléments de la pile
  def size: Int = stack.size

  // Empile un élément sur la pile
  def push( a: A ): Unit = stack.add(a)

  // Dépile et retourne le sommet de la pile
  def pop: A = if( isEmpty ) {
    throw new IllegalStateException("Cannot pop an empty stack")
  } else {
    val last = stack.size - 1
    stack.remove(last)
  }

  //Inverse les deux éléments au sommet de la pile
  //ne fait rien si moins de deux éléments
  def swap: Unit = if( size >= 2 ) {
    val x = pop
    val y = pop
    push(x)
    push(y)
  }

  override def toString = stack.toString

}

object Stack {

  def one[A]( a: A ): Stack[A] = {
    val s = new Stack[A]
    s.push(a)
    s
  }

}

