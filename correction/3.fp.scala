
/*
 * Implémenter les fonctions suivantes en suivant les commentaires.
 * Respectez également les consignes suivantes:
 *  - Toutes les fonctions doivent être pures
 *  - Tout doit être immutable (val au lieu de var)
 *  - Utiliser la recursion terminale si possible
 *  - Utiliser le pattern matching si possible
 */
object Serie3 {

  /*
   * Donne la longueur d'une liste. Votre implémentation ne peut
   *  utiliser aucune fonction de List excepté isEmpty()
   */
  def len[A]( as: List[A] ): Int = {

    def loop( rest: List[A], n: Int ): Int = rest match {
      case Nil => n
      case _ :: t => loop( t, n + 1 )
    }

    loop( as, 0 )
  }


  /*
   * Inverse l'ordre des éléments d'une liste. Votre implémentation ne peut
   * utiliser aucune fonction de List excepté:
   *    - isEmpty
   *    - ::
   *    - head
   *    - tail
   */
  def rev[A]( as: List[A] ): List[A] = {
    def loop( rem: List[A], result: List[A] ): List[A]  = rem match {
      case Nil => result
      case h :: t => loop( t, h :: result )
    }
    loop( as, Nil )
  }

  /*
   * Somme les éléments d'une liste. Votre implémentation ne peut
   * utiliser aucune fonction de List excepté: -
   *    - isEmpty
   *    - head
   *    - tail
   */
  def sum( xs: List[Int] ): Int = {
    def loop( rem: List[Int], n: Int ): Int = rem match {
      case Nil => n
      case h :: t => loop( t, n + h )
    }
    loop(xs, 0)
  }

  /*
   *  Retourne vrai si et seulement si la liste xs ne
   *  comprend que des valeures vraies. Votre implémentation 
   *  ne peutcutiliser aucune fonction de List excepté:
   *    - isEmpty
   *    - head
   *    - tail
   */
  def and_( xs: List[Boolean] ): Boolean  = {
    def loop( rem: List[Boolean], n: Boolean ): Boolean = rem match {
      case Nil => n
      case h :: t => loop( t, n && h )
    }
    loop(xs, true)
  }

  final def and( xs: List[Boolean] ): Boolean  = xs match {
    case Nil => true
    case h :: t if !h => false
    case _ :: t => and(t)
  }


  /*
   *  Applatit une liste. Votre implémentation 
   *  ne peut utiliser aucune fonction de List excepté:
   *   - isEmpty
   *   - head
   *   - tail
   *   - ++
   */
  def flat[A]( las: List[List[A]] ): List[A] =  {
    def loop( rem: List[List[A]], res: List[A]): List[A] = rem match {
      case Nil => res
      case h :: t => loop( t, res ++ h )
    }
    loop(las, Nil)
  }

  /*
   *  Retourne vrai si la Liste a une nombre pair d'éléments. Votre
   *  implémentation ne peut utiliser aucune fonction de List !  Vous
   *  devez utiliser le pattern matching.
   */
  def even[A]( as: List[A] ): Boolean = as match {
    case Nil => true
    case _ :: Nil => false
    case _ :: _ :: rest => even(rest)
  }



}

object Serie3Main extends App {

  import Serie3._

  val is = List( 1, 2, 3, 4, 5 )
  val bs1 = List( true, true, false, true )
  val bs2 = List( true, true, true )
  val las1 = List.empty[List[Int]]
  val las2 = List( List(1,2), List(3), List(4,5) )

  

  require( len(is) == 5 )
  require( len( las1 ) == 0 )
  require( len( bs1 ) == 4 )

  require( rev(is) == List( 5, 4, 3, 2, 1 ) )

  require( rev( bs1 ) == List( true, false, true, true ) )
  require( rev( bs2 ) == bs2 )

  require( sum(is) == 15 )

  require( ! and( bs1 ) )
  require( and( bs2 ) )



  
  require( flat(las1) == Nil )
  require( flat(las2) == is )
  require( sum(flat(las2)) == sum(is) )

 
  require( even( is ) == false )
  require( even( bs1 ) == true )
  require( even( las1 ) == true )

}
