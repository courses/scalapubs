package ch.hepia.tpscala

/* Implémentez les fonctions suivantes. Vous ne pouvez utiliser que les
 *  méthode de 'List' vues dans les exercices précédents.
 */
object HKFunc {

  /* La fonction 'map' applique une fonction 'f' sur chaque élément de
   * la liste 'as'.  La liste résultat doit avoir la même longueur que
   * l'argument.
   */
  def map[A,B]( as: List[A] )( f: A=>B ): List[B] = {
    def loop( rem: List[A], res: List[B] ): List[B] = rem match {
      case Nil => res.reverse
      case a :: t => loop( t, f(a) :: res )
    }
    loop(as, Nil)
  }

  /*
  f = _+1

  loop( List(1,2,3,4), Nil )

  loop( List(2,3,4),   f(1) :: Nil ) == loop( List(2,3,4),   List(2) )

  loop( List(3,4),   f(2) :: List(2) ) == loop( List(3,4), List(3,2) )

  loop( List(4), f(3) :: List(3,2) ) == loop( List(4), List(4,3,2) )

  loop( Nil, f(4) :: List(4,3,2) ) == loop( Nil, List(5,4,3,2) )

  List(5,4,3,2).reverse

  List(2,3,4,5)
   */


  /* La fonction 'filter' utilise le prédicat 'f' pour déterminer quel
   * élément garder. Le résultat peut être vide, mais l'ordre doit
   * être préservé.
   */
  def filter[A]( as: List[A] )( f: A=>Boolean ): List[A] =  {
    def loop( rem: List[A], res: List[A] ): List[A] = rem match {
      case Nil => res.reverse
      case a :: t if f(a) => loop( t, a :: res )
      case _ :: t =>  loop(t,res)
    }
    loop(as, Nil)
  }
    
  
  /* Réduit une liste 'as' en utilisant une opération binaire 'f'.  On
   * supposera que 'as' n'est pas vide.
   */
  def reduce[A]( as: List[A] )( f: (A,A)=>A ): A = {
    def loop(rem: List[A], res: A): A = rem match {
      case Nil => res
      case a :: t => loop( t, f( res, a ) )
    }

    loop(as.tail, as.head)
  }
  


  /* Transforme une fonction 'f' en une fonction s'appliquant sur une
  *  liste. Utiliser la fonction 'map' définie ci-dessus
  */
  def lift[A,B]( f: A=>B ): List[A]=>List[B] = { lstA => map(lstA)(f) }


  //def lift[A,B]( f: A=>B )( lst: List[A] ): List[B] = map(lst)(f)

  //def map[A,B]( lst: List[A] )( f: A=>B ): List[B] = {



  /* DIFFICILE. Transforme une liste 'as' au moyen de la fonction 'f'.
   * Cette fonction est appliquée à chaque élément de 'as' pour
   * produire une nouvelle liste (qui peut être vide). Le résultat est
   * la concaténation de chaque nouvelle liste en respectant l'ordre.
   */

  /*

   def flat[A]( lstlst: List[List[A]] ):List[A] =
    if( lstlst.isEmpty ) Nil
    else reduce(lstlst)( _ ++ _ )
   def bind1[A,B]( as: List[A] )( f: A=>List[B] ): List[B] = 
     flat( map(as)(f) )
   */


  def bind[A,B]( as: List[A] )( f: A=>List[B] ): List[B] = {
    def loop( rem: List[A], res: List[B] ): List[B] = rem match {
      case Nil => res
      case a :: t => loop( t, res ++ f(a) )
    }
    loop(as,Nil)
  }

  /*
  def map[A,B]( as: List[A] )( f: A=>B ): List[B] =
    bind( as )( a => List( f(a) ) )
  def filter[A,B]( as: List[A] )( f: A=>Boolean ): List[A] =
    bind( as )( a => if( f(a) ) List(a) else Nil )
   */
}
